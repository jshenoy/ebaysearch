package com.example.jay.ebaysearch;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Layout;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;


public class eBayList extends ActionBarActivity {

    public JSONObject json;
    public String stv, stv1, stv2, stv3, stv4;
    public String the_keyword;
    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }

    public class HorizontalRulerView extends LinearLayout {

        static final int COLOR = Color.LTGRAY;
        static final int HEIGHT = 4;
        static final int VERTICAL_MARGIN = 10;
        static final int HORIZONTAL_MARGIN = 0;
        static final int TOP_MARGIN = VERTICAL_MARGIN;
        static final int BOTTOM_MARGIN = VERTICAL_MARGIN;
        static final int LEFT_MARGIN = HORIZONTAL_MARGIN;
        static final int RIGHT_MARGIN = HORIZONTAL_MARGIN;

        public HorizontalRulerView(Context context) {
            this(context, null);
        }

        public HorizontalRulerView(Context context, AttributeSet attrs) {
            this(context, attrs, android.R.attr.textViewStyle);
        }

        public HorizontalRulerView(Context context, AttributeSet attrs, int defStyle) {
            super(context, attrs, defStyle);
            setOrientation(VERTICAL);
            View v = new View(context);
            v.setBackgroundColor(COLOR);
            LayoutParams lp = new LayoutParams(
                    LayoutParams.MATCH_PARENT,
                    HEIGHT
            );
            lp.topMargin = TOP_MARGIN;
            lp.bottomMargin = BOTTOM_MARGIN;
            lp.leftMargin = LEFT_MARGIN;
            lp.rightMargin = RIGHT_MARGIN;
            addView(v, lp);
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_e_bay_list);
        try {
            json = new JSONObject(getIntent().getStringExtra("json"));
            the_keyword = getIntent().getStringExtra("the_keyword");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        TableLayout table = (TableLayout) findViewById(R.id.ebayTable);
        table.setColumnShrinkable(1,true);


        TableRow row = new TableRow(this);
        TableRow row1 = new TableRow(this);
        TableRow row2 = new TableRow(this);
        TableRow row3 = new TableRow(this);
        TableRow row4 = new TableRow(this);

        // Create TextView
        TextView tv = new TextView(this);
        TextView tv1 = new TextView(this);
        TextView tv2 = new TextView(this);
        TextView tv3 = new TextView(this);
        TextView tv4 = new TextView(this);

        //String stv, stv1, stv2, stv3, stv4, tmpssc="";
        //stv = stv1 = stv2 = stv3 = stv4 = "Noope";
        String tmpssc = "";
        int[] tlen = new int[5];
        try {
            stv = json.getJSONObject("item0").getJSONObject("basicInfo").getString("title");
            tlen[0] = stv.length();
            stv += "\n\n\n\n\n\t             Price:" + json.getJSONObject("item0").getJSONObject("basicInfo").getString("convertedCurrentPrice");

            tmpssc = json.getJSONObject("item0").getJSONObject("basicInfo").getString("shippingServiceCost");
            if ( tmpssc.compareTo("0.0")== 0 || tmpssc.compareTo("") == 0) {
                stv += "(FREE Shipping)";
            }
            else {
                stv += "(+ "+ tmpssc + " Shipping)";
            }

            stv1 = json.getJSONObject("item1").getJSONObject("basicInfo").getString("title");
            tlen[1] = stv1.length();
            stv1 += "\n\n\n\n\n\t             Price:" + json.getJSONObject("item1").getJSONObject("basicInfo").getString("convertedCurrentPrice");

            tmpssc = json.getJSONObject("item1").getJSONObject("basicInfo").getString("shippingServiceCost");
            if ( tmpssc.compareTo("0.0")== 0 || tmpssc.compareTo("") == 0) {
                stv1 += "(FREE Shipping)";
            }
            else {
                stv1 += "(+ "+ tmpssc + " Shipping)";
            }

            stv2 = json.getJSONObject("item2").getJSONObject("basicInfo").getString("title");
            tlen[2] = stv2.length();
            stv2 +="\n\n\n\n\n\t             Price:" + json.getJSONObject("item2").getJSONObject("basicInfo").getString("convertedCurrentPrice");

            tmpssc = json.getJSONObject("item2").getJSONObject("basicInfo").getString("shippingServiceCost");
            if ( tmpssc.compareTo("0.0")== 0 || tmpssc.compareTo("") == 0) {
                stv2 += "(FREE Shipping)";
            }
            else {
                stv2 += "(+ "+ tmpssc + " Shipping)";
            }


            stv3 = json.getJSONObject("item3").getJSONObject("basicInfo").getString("title");
            tlen[3] = stv3.length();
            stv3 += "\n\n\n\n\n\t             Price:" + json.getJSONObject("item3").getJSONObject("basicInfo").getString("convertedCurrentPrice");

            tmpssc = json.getJSONObject("item2").getJSONObject("basicInfo").getString("shippingServiceCost");
            if ( tmpssc.compareTo("0.0")== 0 || tmpssc.compareTo("") == 0) {
                stv3 += "(FREE Shipping)";
            }
            else {
                stv3 += "(+ "+ tmpssc + " Shipping)";
            }


            stv4 = json.getJSONObject("item4").getJSONObject("basicInfo").getString("title");
            tlen[4] = stv4.length();
            stv4 += "\n\n\n\n\n\t             Price:" + json.getJSONObject("item4").getJSONObject("basicInfo").getString("convertedCurrentPrice");

            tmpssc = json.getJSONObject("item4").getJSONObject("basicInfo").getString("shippingServiceCost");
            if ( tmpssc.compareTo("0.0")== 0 || tmpssc.compareTo("") == 0) {
                stv4 += "(FREE Shipping)";
            }
            else {
                stv4 += "(+ "+ tmpssc + " Shipping)";
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        SpannableString span_stv = new SpannableString(stv);
        span_stv.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),tlen[0]+4,stv.length(),0);
        SpannableString span_stv1 = new SpannableString(stv1);
        span_stv1.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),tlen[1]+4,stv1.length(),0);
        SpannableString span_stv2 = new SpannableString(stv2);
        span_stv2.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),tlen[2]+4,stv2.length(),0);
        SpannableString span_stv3 = new SpannableString(stv3);
        span_stv3.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),tlen[3]+4,stv3.length(),0);
        SpannableString span_stv4 = new SpannableString(stv4);
        span_stv4.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),tlen[4]+4,stv4.length(),0);




        tv.setText(span_stv);
        tv1.setText(span_stv1);
        tv2.setText(span_stv2);
        tv3.setText(span_stv3);
        tv4.setText(span_stv4);


        // create a new ImageView
        ImageView i = new ImageView(this);  i.setClickable(true);
        ImageView i1 = new ImageView(this); i1.setClickable(true);
        ImageView i2 = new ImageView(this); i2.setClickable(true);
        ImageView i3 = new ImageView(this); i3.setClickable(true);
        ImageView i4 = new ImageView(this); i4.setClickable(true);

        // add the TextView and the CheckBox to the new TableRow
        row.addView(i);
        row1.addView(i1);
        row2.addView(i2);
        row3.addView(i3);
        row4.addView(i4);

        row.addView(tv);
        row1.addView(tv1);
        row2.addView(tv2);
        row3.addView(tv3);
        row4.addView(tv4);

        // add the TableRow to the TableLayout
        String galleryURL, galleryURL1, galleryURL2, galleryURL3, galleryURL4;
        galleryURL = galleryURL1 = galleryURL2 = galleryURL3 =
                galleryURL4 = "http://cs-server.usc.edu:45678/hw/hw4/CitrixLogo.png";

        try {
            galleryURL = json.getJSONObject("item0").getJSONObject("basicInfo").getString("galleryURL");
            galleryURL1 = json.getJSONObject("item1").getJSONObject("basicInfo").getString("galleryURL");
            galleryURL2 = json.getJSONObject("item2").getJSONObject("basicInfo").getString("galleryURL");
            galleryURL3 = json.getJSONObject("item3").getJSONObject("basicInfo").getString("galleryURL");
            galleryURL4 = json.getJSONObject("item4").getJSONObject("basicInfo").getString("galleryURL");
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        new DownloadImageTask(i).execute(galleryURL);
        new DownloadImageTask(i1).execute(galleryURL1);
        new DownloadImageTask(i2).execute(galleryURL2);
        new DownloadImageTask(i3).execute(galleryURL3);
        new DownloadImageTask(i4).execute(galleryURL4);

        i.getLayoutParams().height = 300;
        i.getLayoutParams().width = 300;
        i1.getLayoutParams().height = 300;
        i1.getLayoutParams().width = 300;
        i2.getLayoutParams().height = 300;
        i2.getLayoutParams().width = 300;
        i3.getLayoutParams().height = 300;
        i3.getLayoutParams().width = 300;
        i4.getLayoutParams().height = 300;
        i4.getLayoutParams().width = 300;

        // Image listeners
        i.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                String url = "";
                try {
                    url = json.getJSONObject("item0").getJSONObject("basicInfo").getString("viewItemURL");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(browserIntent);
            }
        });
        i1.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                String url = "";
                try {
                    url = json.getJSONObject("item1").getJSONObject("basicInfo").getString("viewItemURL");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(browserIntent);
            }
        });
        i2.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                String url = "";
                try {
                    url = json.getJSONObject("item2").getJSONObject("basicInfo").getString("viewItemURL");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(browserIntent);
            }
        });
        i3.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                String url = "";
                try {
                    url = json.getJSONObject("item3").getJSONObject("basicInfo").getString("viewItemURL");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(browserIntent);
            }
        });
        i4.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                String url = "";
                try {
                    url = json.getJSONObject("item4").getJSONObject("basicInfo").getString("viewItemURL");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(browserIntent);
            }
        });


        // Text listeners

        tv.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                Intent intent = new Intent(eBayList.this, DetailsActivity.class);
                intent.putExtra("json", json.toString());
                intent.putExtra("thisitem", "item0");
                intent.putExtra("priceinfo", stv);
                eBayList.this.startActivity(intent);
            }
        });
        tv1.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                Intent intent = new Intent(eBayList.this, DetailsActivity.class);
                intent.putExtra("json", json.toString());
                intent.putExtra("thisitem", "item1");
                intent.putExtra("priceinfo", stv1);
                eBayList.this.startActivity(intent);
            }
        });
        tv2.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                Intent intent = new Intent(eBayList.this, DetailsActivity.class);
                intent.putExtra("json", json.toString());
                intent.putExtra("thisitem", "item2");
                intent.putExtra("priceinfo", stv2);
                eBayList.this.startActivity(intent);
            }
        });
        tv3.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                Intent intent = new Intent(eBayList.this, DetailsActivity.class);
                intent.putExtra("json", json.toString());
                intent.putExtra("thisitem", "item3");
                intent.putExtra("priceinfo", stv3);
                eBayList.this.startActivity(intent);
            }
        });
        tv4.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                Intent intent = new Intent(eBayList.this, DetailsActivity.class);
                intent.putExtra("json", json.toString());
                intent.putExtra("thisitem", "item4");
                intent.putExtra("priceinfo", stv4);
                eBayList.this.startActivity(intent);
            }
        });

        // Write the results into result count view
        TextView resultcv = (TextView)findViewById(R.id.resultCountView);
        resultcv.setText("Results for '"+the_keyword+"'");


        HorizontalRulerView hrv1 = new HorizontalRulerView(this);
        HorizontalRulerView hrv2 = new HorizontalRulerView(this);

        HorizontalRulerView hrv3 = new HorizontalRulerView(this);
        HorizontalRulerView hrv4 = new HorizontalRulerView(this);

        HorizontalRulerView hrv5 = new HorizontalRulerView(this);
        HorizontalRulerView hrv6 = new HorizontalRulerView(this);

        HorizontalRulerView hrv7 = new HorizontalRulerView(this);
        HorizontalRulerView hrv8 = new HorizontalRulerView(this);

        HorizontalRulerView hrv9 = new HorizontalRulerView(this);
        HorizontalRulerView hrv10 = new HorizontalRulerView(this);

        TableRow rowx1 = new TableRow(this);
        TableRow rowx2 = new TableRow(this);
        TableRow rowx3 = new TableRow(this);
        TableRow rowx4 = new TableRow(this);
        TableRow rowx5 = new TableRow(this);

        rowx1.addView(hrv1);
        rowx1.addView(hrv2);

        rowx2.addView(hrv3);
        rowx2.addView(hrv4);

        rowx3.addView(hrv5);
        rowx3.addView(hrv6);

        rowx4.addView(hrv7);
        rowx4.addView(hrv8);

        rowx5.addView(hrv9);
        rowx5.addView(hrv10);


        table.addView(rowx1,new TableLayout.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));


        table.addView(row,new TableLayout.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));

        table.addView(rowx2,new TableLayout.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));

        table.addView(row1,new TableLayout.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));

        table.addView(rowx3,new TableLayout.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));

        table.addView(row2,new TableLayout.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));

        table.addView(rowx4,new TableLayout.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));

        table.addView(row3,new TableLayout.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));

        table.addView(rowx5,new TableLayout.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));

        table.addView(row4,new TableLayout.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_e_bay_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
