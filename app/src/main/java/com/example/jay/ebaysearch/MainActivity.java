package com.example.jay.ebaysearch;

import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.*;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import static java.lang.Integer.parseInt;


public class MainActivity extends ActionBarActivity {

    private static final int TIMEOUT_MILLISEC = 3000;
    public String the_keyword;


    private class DownloadFilesTask extends AsyncTask<URL, Integer, Integer> {

        TextView val;

        @Override
        protected void onPreExecute() {
            val = (TextView) findViewById(R.id.textView6);
        }

        protected Integer doInBackground(URL... url) {
            HttpClient httpclient = new DefaultHttpClient();

            // Prepare a request object
            String urlstring = url[0].toString();
            HttpGet httpget = new HttpGet(urlstring);
            // Execute the request
            HttpResponse response;
            try {
                response = httpclient.execute(httpget);

                // Get hold of the response entity
                HttpEntity entity = response.getEntity();
                // If the response does not enclose an entity, there is no need
                // to worry about connection release

                if (entity != null) {

                    // A Simple JSON Response Read
                    InputStream instream = entity.getContent();

                    BufferedReader streamReader = new BufferedReader(new InputStreamReader(instream));
                    StringBuilder responseStrBuilder = new StringBuilder();

                    String inputStr;
                    while ((inputStr = streamReader.readLine()) != null)
                        responseStrBuilder.append(inputStr);
                    JSONObject json = new JSONObject(responseStrBuilder.toString());
                    //Log.v("JSON response:",json.toString() );
                    instream.close();

                    // Wait! Validate json. If no results just return
                    if (json.getString("ack").compareTo("No results found") == 0){
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                String mes = "No Results found!";
                                SpannableString smes = new SpannableString(mes);
                                smes.setSpan(new ForegroundColorSpan(Color.BLUE), 0, mes.length(), 0);
                                val.setText(smes);
                            }
                        });
                        return -1;
                    }


                    Intent intent = new Intent(MainActivity.this, eBayList.class);
                    intent.putExtra("json", json.toString());
                    intent.putExtra("the_keyword", the_keyword);
                    MainActivity.this.startActivity(intent);
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }

            return 0;
        }

        @Override
        protected void onPostExecute(Integer integer) {
        }
    }

    public static boolean isNumeric(String str)
    {
        try
        {
            float d = Float.parseFloat(str);
        }
        catch(NumberFormatException nfe)
        {
            return false;
        }
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        Spinner spinner = (Spinner) findViewById(R.id.spinner);

        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
        R.array.sortby, android.R.layout.simple_spinner_item);

        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);

        final Button clickButton = (Button) findViewById(R.id.button2);
        clickButton.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                EditText keyword = (EditText) findViewById(R.id.editText);
                EditText from = (EditText) findViewById(R.id.editText2);
                EditText to = (EditText) findViewById(R.id.editText3);
                Spinner mySpinner=(Spinner) findViewById(R.id.spinner);
                String spinner_text = mySpinner.getSelectedItem().toString();

                if (spinner_text.equals("Price: highest first")) {
                    spinner_text = "CurrentPriceHighest";
                }
                else if (spinner_text.equals("Price + Shipping: highest first")) {
                    spinner_text = "PricePlusShippingHighest";
                }
                else if (spinner_text.equals("Price + Shipping: lowest first")) {
                    spinner_text = "PricePlusShippingLowest";
                }
                else {
                    spinner_text = "BestMatch";
                }

                URL url = null;
                String reqstring = "http://jayavanth.elasticbeanstalk.com/hw8.php?keyword=";
                String strkw,strkwtrim, strto, strtotrim, strfm, strfmtrim;

                strkw = keyword.getText().toString();
                strkwtrim = strkw.trim();

                strfm = from.getText().toString();
                strfmtrim = strfm.trim();

                strto = to.getText().toString();
                strtotrim = strto.trim();


                ////////////////////////////////////////////////////////////////////////////////////
                //                                  VALIDATIONS
                ////////////////////////////////////////////////////////////////////////////////////
                TextView valtext = (TextView) findViewById(R.id.textView6);

                if (strkw.compareTo("") == 0 ) {
                    valtext.setText("The keyword must not be empty");
                    return;
                }
                if (!(strfm.compareTo("") == 0)) {
                    if (!isNumeric(strfm)) {
                        valtext.setText("Must be a positive integer or decimal number");
                        return;
                    }
                }
                if (!(strto.compareTo("") == 0)) {
                    if (!isNumeric(strto)) {
                        valtext.setText("Must be a positive integer or decimal number");
                        return;
                    }
                }
                if (!(strfm.compareTo("") == 0) && !(strto.compareTo("") == 0)) {
                        if (parseInt(strto) < parseInt(strfm)) {
                            valtext.setText("Price To must not be less than Price From");
                            return;
                        }
                }

                reqstring += URLEncoder.encode(strkwtrim);
                reqstring += "&from=" + strfmtrim;
                reqstring += "&to=" + strtotrim;
                reqstring += "&sortby=" + spinner_text;
                reqstring += "&resultsperpage=5&submit=Submit";
                try {
                    url = new URL(reqstring);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                the_keyword = strkwtrim;
                new DownloadFilesTask().execute(url);

            }
        });

        final Button clearButton = (Button) findViewById(R.id.button);
        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText kw = (EditText) findViewById(R.id.editText);
                EditText fr = (EditText) findViewById(R.id.editText2);
                EditText to = (EditText) findViewById(R.id.editText3);

                Spinner sb = (Spinner) findViewById(R.id.spinner);

                TextView val = (TextView) findViewById(R.id.textView6);

                kw.setText("");
                fr.setText("");
                to.setText("");

                sb.setSelection(0);

                val.setText("");


            }
        });

        EditText searching = (EditText) findViewById(R.id.editText);
        searching.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    clickButton.performClick();
                    return true;
                }
                return false;
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
