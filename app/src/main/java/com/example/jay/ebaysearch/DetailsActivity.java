package com.example.jay.ebaysearch;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.ImageSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;


public class DetailsActivity extends ActionBarActivity {
    public JSONObject json;
    public String item;
    CallbackManager callbackManager;
    ShareDialog shareDialog;
    LoginManager loginManager;



    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }

    static String splitCamelCase(String s) {
        return s.replaceAll(
                String.format("%s|%s|%s",
                        "(?<=[A-Z])(?=[A-Z][a-z])",
                        "(?<=[^A-Z])(?=[A-Z])",
                        "(?<=[A-Za-z])(?=[^A-Za-z])"
                ),
                ", "
        );
    }

//    static String spaceAdder(String s) {
//        String spaceadded;
//        spaceadded = new StringBuilder(s).insert(26, " ").toString();
//        return spaceadded;
//    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Initialize fb sdk
        FacebookSdk.sdkInitialize(getApplicationContext());

        setContentView(R.layout.activity_details);

        try {
            json = new JSONObject(getIntent().getStringExtra("json"));
            item = new String(getIntent().getStringExtra("thisitem"));
        } catch (JSONException e) {
            e.printStackTrace();
        }



        // Some declarations
        String NA = "N/A";
        String title =  "No title!!";
        String location = "Chikmagalur";
        String toprated = "false";
        String buynow = "No product link";
        String priceinfo ="Price: ";
        String tmpssc = null;

        // Put the image
        View iv = findViewById(R.id.imageView);
        String superimg_url = "";
        String galleryURL = "";


        try {
            superimg_url = json.getJSONObject(item).getJSONObject("basicInfo").getString("pictureURLSuperSize");
            title = json.getJSONObject(item).getJSONObject("basicInfo").getString("title");
            location = json.getJSONObject(item).getJSONObject("basicInfo").getString("location");
            toprated = json.getJSONObject(item).getJSONObject("basicInfo").getString("topRatedListing");
            buynow = json.getJSONObject(item).getJSONObject("basicInfo").getString("viewItemURL");
            tmpssc = json.getJSONObject(item).getJSONObject("basicInfo").getString("shippingServiceCost");
            galleryURL = json.getJSONObject(item).getJSONObject("basicInfo").getString("galleryURL");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        if(superimg_url.compareTo("") == 0) {
            superimg_url = galleryURL;
        }
        new DownloadImageTask((ImageView) iv).execute(superimg_url);



        ////////////////////////////////////////////////////////////////////////////////////////////


        // Put the description : Title is in (String)title, Location is in (String)location
        // Toprated is in (String) toprated


        if ( tmpssc.compareTo("0.0")== 0 || tmpssc.compareTo("") == 0) {
            priceinfo += "(FREE Shipping)";
        }
        else {
            priceinfo += "(+ "+ tmpssc + " Shipping)";
        }


        TextView descview = (TextView) findViewById(R.id.descriptionID);


        SpannableString span_title = new SpannableString(title);
        SpannableString span_priceinfo = new SpannableString(priceinfo);
        SpannableString span_location = new SpannableString(location);

        span_title.setSpan(new RelativeSizeSpan(0.9f),0,title.length(),0);
        span_title.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),0,title.length(),0);
        span_title.setSpan(new ForegroundColorSpan(Color.BLACK),0,title.length(), 0);


        span_priceinfo.setSpan(new RelativeSizeSpan(0.6f),0,priceinfo.length(),0);
        span_priceinfo.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),0,priceinfo.length(),0);

        span_location.setSpan(new RelativeSizeSpan(0.6f),0,location.length(),0);


        descview.setText(TextUtils.concat(span_title,"\n",span_priceinfo,"\n",span_location));

        // Change the toprated picture if it's toprated

        ImageView iv_toprated = (ImageView) findViewById(R.id.imageView2);

        if (toprated.compareTo("true") == 0) {
            String topratedURI = "@drawable/top_rated";
            int imageResource = getResources().getIdentifier(topratedURI, null, getPackageName());
            Drawable res = getResources().getDrawable(imageResource);
            iv_toprated.setImageDrawable(res);
        }

        ////////////////////////////////////////////////////////////////////////////////////////////
        //  MAKE TABLE
        ////////////////////////////////////////////////////////////////////////////////////////////

        //////////////////////////////////////// BASIC INFO ////////////////////////////////////////
        TableLayout bi_pop = (TableLayout) findViewById(R.id.basicInfoTextView);

        String bi_catname = "Category Name                   ";
        String bi_conditi = "Condition";
        String bi_bformat = "Buying Format";
        String bi_cat, bi_con, bi_buf;
                bi_cat = bi_con = bi_buf = "";
        String newline3 = "\n\n\n";
        String newline2 = "\n\n";

        // Get basic Info
        try {
            bi_cat = json.getJSONObject(item).getJSONObject("basicInfo").getString("categoryName");
            bi_con = json.getJSONObject(item).getJSONObject("basicInfo").getString("conditionDisplayName");
            bi_buf = json.getJSONObject(item).getJSONObject("basicInfo").getString("listingType");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ////////////////////////////////////////////////////////////////////////////////////////////
        //                                          CHECK FOR EMPTY
        ////////////////////////////////////////////////////////////////////////////////////////////
        if(bi_cat.compareTo("") == 0) {
            bi_cat = NA;
        }
        if(bi_con.compareTo("") == 0) {
            bi_con = NA;
        }
        if(bi_buf.compareTo("") == 0) {
            bi_buf = NA;
        }
        ////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////



        SpannableString sbi_cat = new SpannableString(bi_cat);
        SpannableString sbi_con = new SpannableString(bi_con);
        SpannableString sbi_buf = new SpannableString(bi_buf);


        SpannableString sbi_catname = new SpannableString(bi_catname);
        SpannableString sbi_conditi = new SpannableString(bi_conditi);
        SpannableString sbi_bformat = new SpannableString(bi_bformat);

        sbi_catname.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),0,bi_catname.length(),0);
        sbi_conditi.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),0,bi_conditi.length(),0);
        sbi_bformat.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),0,bi_bformat.length(),0);

        TableRow row_cat = new TableRow(this);
        TableRow row_con = new TableRow(this);
        TableRow row_buf = new TableRow(this);

        TextView tv_catname = new TextView(this);
        TextView tv_conditi = new TextView(this);
        TextView tv_bformat = new TextView(this);
        TextView tv_cat = new TextView(this);
        TextView tv_con = new TextView(this);
        TextView tv_buf = new TextView(this);


        View spacer = new View(this);
        View spacer2 = new View(this);
        View spacer3 = new View(this);

        tv_catname.setText(sbi_catname);
        tv_conditi.setText(sbi_conditi);
        tv_bformat.setText(sbi_bformat);
        tv_cat.setText(sbi_cat);
        tv_con.setText(sbi_con);
        tv_buf.setText(sbi_buf);


        row_cat.addView(tv_catname);
        row_cat.addView(tv_cat);
        row_cat.addView(spacer, new TableRow.LayoutParams(1, 130));

        row_con.addView(tv_conditi);
        row_con.addView(tv_con);
        row_con.addView(spacer2, new TableRow.LayoutParams(1, 130));

        row_buf.addView(tv_bformat);
        row_buf.addView(tv_buf);
        row_buf.addView(spacer3, new TableRow.LayoutParams(1, 130));

        bi_pop.addView(row_cat,new TableLayout.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));
        bi_pop.addView(row_con,new TableLayout.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));
        bi_pop.addView(row_buf,new TableLayout.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));


        ////////////////////////////////////////// SELLER //////////////////////////////////////////
        TableLayout si_pop = (TableLayout) findViewById(R.id.sellerTextView);

        String si_un, si_fs, si_ps, si_fr, si_tr, si_s;
                si_un = si_fs = si_ps = si_fr = si_tr = si_s = "";


        try {
            si_un = json.getJSONObject(item).getJSONObject("sellerInfo").getString("sellerUserName");
            si_fs = json.getJSONObject(item).getJSONObject("sellerInfo").getString("feedbackScore");
            si_ps = json.getJSONObject(item).getJSONObject("sellerInfo").getString("positiveFeedbackPercent");
            si_fr = json.getJSONObject(item).getJSONObject("sellerInfo").getString("feedbackRatingStar");
            si_tr = json.getJSONObject(item).getJSONObject("sellerInfo").getString("topRatedSeller");
            si_s = json.getJSONObject(item).getJSONObject("sellerInfo").getString("storeName");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ////////////////////////////////////////////////////////////////////////////////////////////
        //                                          CHECK FOR EMPTY
        ////////////////////////////////////////////////////////////////////////////////////////////
        if(si_un.compareTo("") == 0) {
            si_un = NA;
        }
        if(si_fs.compareTo("") == 0) {
            si_fs = NA;
        }
        if(si_ps.compareTo("") == 0) {
            si_ps = NA;
        }
        if(si_fr.compareTo("") == 0) {
            si_fr = NA;
        }
        if(si_s.compareTo("") == 0) {
            si_s = NA;
        }
        ////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////



        SpannableString ssi_un = new SpannableString(si_un);
        SpannableString ssi_fs = new SpannableString(si_fs);
        SpannableString ssi_ps = new SpannableString(si_ps);
        SpannableString ssi_fr = new SpannableString(si_fr);
        SpannableString ssi_tr;
        SpannableString ssi_s = new SpannableString(si_s);

        String si_uname = "User Name                     ";
        String si_feeds = "Feedback Score";
        String si_pfeed = "Positive Feedback";
        String si_frate = "Feedback Rating";
        String si_trate = "Top Rated";
        String si_store = "Store";


        SpannableString ssi_uname = new SpannableString(si_uname);
        SpannableString ssi_feeds = new SpannableString(si_feeds);
        SpannableString ssi_pfeed = new SpannableString(si_pfeed);
        SpannableString ssi_frate = new SpannableString(si_frate);
        SpannableString ssi_trate = new SpannableString(si_trate);
        SpannableString ssi_store = new SpannableString(si_store);


        ssi_uname.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),0,si_uname.length(),0);
        ssi_feeds.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),0,si_feeds.length(),0);
        ssi_pfeed.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),0,si_pfeed.length(),0);
        ssi_frate.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),0,si_frate.length(),0);
        ssi_trate.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),0,si_trate.length(),0);
        ssi_store.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),0,si_store.length(),0);

        Bitmap check = BitmapFactory.decodeResource( getResources(), R.drawable.check_mark3 );
        Bitmap cross = BitmapFactory.decodeResource( getResources(), R.drawable.cross_mark3 );


        String abc = "   ";
        String def= "   ";

        SpannableString sabc = new SpannableString(abc);
        SpannableString sdef = new SpannableString(def);

        sabc.setSpan(new ImageSpan( check ), 0, 3, Spannable.SPAN_INCLUSIVE_INCLUSIVE );
        sdef.setSpan(new ImageSpan( cross ), 0, 3, Spannable.SPAN_INCLUSIVE_INCLUSIVE );



        if (si_tr.compareTo("true") == 0) {
            ssi_tr = sabc;
        }
        else {
            ssi_tr = sdef;
        }


        TableRow row_un = new TableRow(this);
        TableRow row_fs = new TableRow(this);
        TableRow row_ps = new TableRow(this);
        TableRow row_fr = new TableRow(this);
        TableRow row_tr = new TableRow(this);
        TableRow row_s = new TableRow(this);


        TextView tv_uname = new TextView(this);
        TextView tv_feeds = new TextView(this);
        TextView tv_pfeed= new TextView(this);
        TextView tv_frate = new TextView(this);
        TextView tv_trate = new TextView(this);
        TextView tv_store = new TextView(this);


        TextView tv_un = new TextView(this);
        TextView tv_fs = new TextView(this);
        TextView tv_ps = new TextView(this);
        TextView tv_fr = new TextView(this);
        TextView tv_tr = new TextView(this);
        TextView tv_s  = new TextView(this);


        View selspacer = new View(this);
        View selspacer2 = new View(this);
        View selspacer3 = new View(this);
        View selspacer4 = new View(this);
        View selspacer5 = new View(this);
        View selspacer6 = new View(this);

        tv_uname.setText(ssi_uname);
        tv_feeds.setText(ssi_feeds);
        tv_pfeed.setText(ssi_pfeed);
        tv_frate.setText(ssi_frate);
        tv_trate.setText(ssi_trate);
        tv_store.setText(ssi_store);

        tv_un.setText(ssi_un);
        tv_fs.setText(ssi_fs);
        tv_ps.setText(ssi_ps);
        tv_fr.setText(ssi_fr);
        tv_tr.setText(ssi_tr);
        tv_s.setText(ssi_s);

        row_un.addView(tv_uname);
        row_un.addView(tv_un);
        row_un.addView(selspacer, new TableRow.LayoutParams(1, 130));

        row_fs.addView(tv_feeds);
        row_fs.addView(tv_fs);
        row_fs.addView(selspacer2, new TableRow.LayoutParams(1, 130));

        row_ps.addView(tv_pfeed);
        row_ps.addView(tv_ps);
        row_ps.addView(selspacer3, new TableRow.LayoutParams(1, 130));


        row_fr.addView(tv_frate);
        row_fr.addView(tv_fr);
        row_fr.addView(selspacer4, new TableRow.LayoutParams(1, 130));

        row_tr.addView(tv_trate);
        row_tr.addView(tv_tr);
        row_tr.addView(selspacer5, new TableRow.LayoutParams(1, 130));

        row_s.addView(tv_store);
        row_s.addView(tv_s);
        row_s.addView(selspacer6, new TableRow.LayoutParams(1, 130));


        si_pop.addView(row_un ,new TableLayout.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));
        si_pop.addView(row_fs ,new TableLayout.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));
        si_pop.addView(row_ps ,new TableLayout.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));
        si_pop.addView(row_fr ,new TableLayout.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));
        si_pop.addView(row_tr ,new TableLayout.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));
        si_pop.addView(row_s  ,new TableLayout.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));


        ///////////////////////////////////////// SHIPPING /////////////////////////////////////////
        TableLayout shi_pop = (TableLayout) findViewById(R.id.shippingTextView);
        String shi_st, shi_ht, shi_sl, shi_es, shi_od, shi_ra;
               shi_st = shi_ht = shi_sl = shi_es = shi_od = shi_ra = "";

        try {
            shi_st  = json.getJSONObject(item).getJSONObject("shippingInfo").getString("shippingType");
            shi_ht  = json.getJSONObject(item).getJSONObject("shippingInfo").getString("handlingTime");
            shi_sl  = json.getJSONObject(item).getJSONObject("shippingInfo").getString("shipToLocations");
            shi_es  = json.getJSONObject(item).getJSONObject("shippingInfo").getString("expeditedShipping");
            shi_od  = json.getJSONObject(item).getJSONObject("shippingInfo").getString("oneDayShippingAvailable");
            shi_ra = json.getJSONObject(item).getJSONObject("shippingInfo").getString("returnsAccepted");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        shi_st = splitCamelCase(shi_st);
        ////////////////////////////////////////////////////////////////////////////////////////////
        //                                          CHECK FOR EMPTY
        ////////////////////////////////////////////////////////////////////////////////////////////
        if(shi_st.compareTo("") == 0) {
            shi_st = NA;
        }
        if(shi_ht.compareTo("") == 0) {
            shi_ht = NA;
        }
        if(shi_sl.compareTo("") == 0) {
            shi_sl = NA;
        }
        ////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////
        SpannableString sshi_st = new SpannableString(shi_st);
        SpannableString sshi_ht = new SpannableString(shi_ht);
        SpannableString sshi_sl = new SpannableString(shi_sl);
        SpannableString sshi_es;
        SpannableString sshi_od;
        SpannableString sshi_ra;


        String shi_shipt = "Shipping Type                   ";
        String shi_htime = "Handling Time";
        String shi_slocs = "Shipping Locations";
        String shi_expsh = "Expedited Shipping";
        String shi_ondsh = "One Day Shipping";
        String shi_retac = "Returns Accepted";

        SpannableString sshi_shipt = new SpannableString(shi_shipt);
        SpannableString sshi_htime = new SpannableString(shi_htime);
        SpannableString sshi_slocs = new SpannableString(shi_slocs);
        SpannableString sshi_expsh = new SpannableString(shi_expsh);
        SpannableString sshi_ondsh = new SpannableString(shi_ondsh);
        SpannableString sshi_retac = new SpannableString(shi_retac);

        sshi_shipt.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),0,shi_shipt.length(),0);
        sshi_htime.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),0,shi_htime.length(),0);
        sshi_slocs.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),0,shi_slocs.length(),0);
        sshi_expsh.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),0,shi_expsh.length(),0);
        sshi_ondsh.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),0,shi_ondsh.length(),0);
        sshi_retac.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),0,shi_retac.length(),0);

        // Some shitty resource limitation
        Bitmap crosses = BitmapFactory.decodeResource( getResources(), R.drawable.cross_mark3 );
        Bitmap checkes = BitmapFactory.decodeResource( getResources(), R.drawable.check_mark3 );
        Bitmap crossod = BitmapFactory.decodeResource( getResources(), R.drawable.cross_mark3 );
        Bitmap checkod = BitmapFactory.decodeResource( getResources(), R.drawable.check_mark3 );

        String abces = "   ";
        String defes = "   ";
        String abcod = "   ";
        String defod = "   ";

        SpannableString sabces = new SpannableString(abces);
        SpannableString sdefes = new SpannableString(defes);
        SpannableString sabcod = new SpannableString(abcod);
        SpannableString sdefod = new SpannableString(defod);

        sabces.setSpan(new ImageSpan( checkes ), 0, 3, Spannable.SPAN_INCLUSIVE_INCLUSIVE );
        sdefes.setSpan(new ImageSpan( crosses ), 0, 3, Spannable.SPAN_INCLUSIVE_INCLUSIVE );
        sabcod.setSpan(new ImageSpan( checkod ), 0, 3, Spannable.SPAN_INCLUSIVE_INCLUSIVE );
        sdefod.setSpan(new ImageSpan( crossod ), 0, 3, Spannable.SPAN_INCLUSIVE_INCLUSIVE );

        if (shi_es.compareTo("true") == 0) {
            sshi_es = sabces;
        }
        else {
            sshi_es = sdefes;
        }
        if (shi_od.compareTo("true") == 0) {
            sshi_od = sabcod;
        }
        else {
            sshi_od = sdefod;
        }
        if (shi_ra.compareTo("true") == 0) {
            sshi_ra = sabc;
        }
        else {
            sshi_ra = sdef;
        }

        TableRow row_st = new TableRow(this);
        TableRow row_ht = new TableRow(this);
        TableRow row_sl = new TableRow(this);
        TableRow row_es = new TableRow(this);
        TableRow row_od = new TableRow(this);
        TableRow row_ra= new TableRow(this);


        TextView tv_shipt = new TextView(this);
        TextView tv_htime = new TextView(this);
        TextView tv_slocs= new TextView(this);
        TextView tv_expsh = new TextView(this);
        TextView tv_ondsh = new TextView(this);
        TextView tv_retac = new TextView(this);


        TextView tv_st = new TextView(this);
        TextView tv_ht = new TextView(this);
        TextView tv_sl = new TextView(this);
        TextView tv_es = new TextView(this);
        TextView tv_od = new TextView(this);
        TextView tv_ra = new TextView(this);


        View shispacer = new View(this);
        View shispacer2 = new View(this);
        View shispacer3 = new View(this);
        View shispacer4 = new View(this);
        View shispacer5 = new View(this);
        View shispacer6 = new View(this);

        tv_shipt.setText(sshi_shipt);
        tv_htime.setText(sshi_htime);
        tv_slocs.setText(sshi_slocs);
        tv_expsh.setText(sshi_expsh);
        tv_ondsh.setText(sshi_ondsh);
        tv_retac.setText(sshi_retac);

        tv_st.setText(sshi_st);
        tv_ht.setText(sshi_ht);
        tv_sl.setText(sshi_sl);
        tv_es.setText(sshi_es);
        tv_od.setText(sshi_od);
        tv_ra.setText(sshi_ra);

        row_st.addView(tv_shipt);
        row_st.addView(tv_st);
        row_st.addView(shispacer, new TableRow.LayoutParams(1, 130));


        row_ht.addView(tv_htime);
        row_ht.addView(tv_ht);
        row_ht.addView(shispacer2, new TableRow.LayoutParams(1, 130));

        row_sl.addView(tv_slocs);
        row_sl.addView(tv_sl);
        row_sl.addView(shispacer3, new TableRow.LayoutParams(1, 130));


        row_es.addView(tv_expsh);
        row_es.addView(tv_es);
        row_es.addView(shispacer4, new TableRow.LayoutParams(1, 130));

        row_od.addView(tv_ondsh);
        row_od.addView(tv_od);
        row_od.addView(shispacer5, new TableRow.LayoutParams(1, 130));

        row_ra.addView(tv_retac);
        row_ra.addView(tv_ra);
        row_ra.addView(shispacer6, new TableRow.LayoutParams(1, 130));


        shi_pop.addView(row_st ,new TableLayout.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));
        shi_pop.addView(row_ht ,new TableLayout.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));
        shi_pop.addView(row_sl ,new TableLayout.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));
        shi_pop.addView(row_es ,new TableLayout.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));
        shi_pop.addView(row_od ,new TableLayout.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));
        shi_pop.addView(row_ra ,new TableLayout.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));

        ////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////////////////////////
        // Facebook
        ////////////////////////////////////////////////////////////////////////////////////////////




        ImageView fbimage = (ImageView) findViewById(R.id.imageView4);
        fbimage.setClickable(true);

        shareDialog = new ShareDialog(this);
        callbackManager = CallbackManager.Factory.create();


        // Temp variables
        final String finalBuynow = buynow;
        final String finalGalleryURL = galleryURL;
        final String finalTitle = title;
        final String fbdetails = priceinfo + ", " + location;

        // Onclick for fb

        fbimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {



                    @Override
                    public void onSuccess(Sharer.Result sr) {
                        if (sr.getPostId() == null) {
                            Toast.makeText(getApplicationContext(), "Post Cancelled",
                                    Toast.LENGTH_SHORT).show();
                        }
                        else {
                            Toast.makeText(getApplicationContext(), "Posted Story, ID:" + sr.getPostId(),
                                    Toast.LENGTH_SHORT).show();
                        }
                    }


                    @Override
                    public void onCancel() {
                        Toast.makeText(getApplicationContext(), "Post Cancelled",
                                Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(FacebookException fe) {
                        Toast.makeText(getApplicationContext(), "Failure because: "
                                + fe.getMessage(), Toast.LENGTH_LONG).show();
                    }


                });
                if (ShareDialog.canShow(ShareLinkContent.class)) {
                    ShareLinkContent content = new ShareLinkContent.Builder()
                            .setContentUrl(Uri.parse(finalBuynow))
                            .setImageUrl(Uri.parse(finalGalleryURL))
                            .setContentTitle(finalTitle)
                            .setContentDescription(fbdetails)
                            .build();

                    shareDialog.show(content);
                }

            }
        });



        ////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////

        // Default behavior
        final Button bi = (Button) findViewById(R.id.button3);
        final Button si = (Button) findViewById(R.id.button4);
        final Button shi = (Button) findViewById(R.id.button5);
        bi.setPressed(true); // Or setSelected(true)

        // Populate the views
        final TableLayout bi_text = (TableLayout) findViewById(R.id.basicInfoTextView);
        final TableLayout si_text = (TableLayout) findViewById(R.id.sellerTextView);
        final TableLayout shi_text = (TableLayout) findViewById(R.id.shippingTextView);

        bi.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent me) {
                bi.setPressed(true);
                si.setPressed(false);
                shi.setPressed(false);


                si_text.setVisibility(View.GONE);
                shi_text.setVisibility(View.GONE);

                bi_text.setVisibility(View.VISIBLE);
                return true;
            }
        });

        si.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent me) {
                bi.setPressed(false);
                si.setPressed(true);
                shi.setPressed(false);


                bi_text.setVisibility(View.GONE);
                shi_text.setVisibility(View.GONE);

                si_text.setVisibility(View.VISIBLE);
                return true;
            }
        });

        shi.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent me) {
                bi.setPressed(false);
                si.setPressed(false);
                shi.setPressed(true);

                si_text.setVisibility(View.GONE);
                bi_text.setVisibility(View.GONE);

                shi_text.setVisibility(View.VISIBLE);
                return true;
            }
        });

        final ImageView buynowbutton = (ImageView) findViewById(R.id.imageView3);
        buynowbutton.setClickable(true);

        buynowbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(finalBuynow));
                startActivity(browserIntent);
            }
        });


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
